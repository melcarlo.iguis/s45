import {useEffect, useContext} from 'react'
import {Redirect } from 'react-router-dom'
import UserContext from '../UserContext'

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext)	

	unsetUser();

	useEffect(() => {
		// set the user state back to its og value
		setUser({id:null});
	})

	return(
		<Redirect to='/login'/>
	)
}