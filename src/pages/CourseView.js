// When viewing single course details.
import {useState, useEffect,useContext} from 'react'
import {Container,Row,Col,Button, Card} from 'react-bootstrap'
// useHistory for v5 down in react-router-dom
import {useParams, useHistory, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function CourseView (){

	// const {courseId,userId} = useParams()
	const {courseId} = useParams()

	const {user} = useContext(UserContext)

	// useHistory or useNavigate
	const history = useHistory()
	const userId = ''

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)




	const enroll = (courseId) => {
		console.log(user)
		fetch('http://localhost:4000/users/enroll',{
			method: 'POST',
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: user.id
			})
		})
		.then(res => res.json())
		.then(data=> {

			console.log(data)

			if(data=== true){
				Swal.fire({
					title: 'Enrolled successfully',
					icon: 'success',
					text: 'You have successfully enrolled in this course'
				})

				history.push("/courses")

			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})

				history.push("/courses")
			}
		})
	}

	useEffect(() => {
		console.log(courseId)

		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)


			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	},[courseId,userId])


	return(
		<Container className = "mt-5">
			<Row>
				<Col lg = {{span: 6, offset: 3}}>
					<Card>
						<Card.Body className = "text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text> Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>5:30pm to 9:30pm</Card.Text>

							{user.id !== null ?
								<Button variant = "primary" onClick={()=>enroll(courseId)}>Enroll</Button>
								:
								<Link clasName="btn btn-danger btn-block" to="/login">Log in to Enroll</Link>
							}

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
