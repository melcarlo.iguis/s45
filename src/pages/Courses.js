import {Fragment, useEffect,useState} from 'react'
import CourseCard from '../components/CourseCard'
import SearchIcon from '@mui/icons-material/Search';
import './SearchBar.css'
// import UserContext from '../UserContext'

export default function Courses(){

	
	const [courses, setCourses] = useState([])

    const [wordEntered, setWordEntered] = useState("");


	// I add useEffect to fetch all courses data to have an initial value
	useEffect(()=>{
		fetch('http://localhost:4000/courses/all')
		.then(res=> res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return (

					<CourseCard key = {course.id} courseProp = {course}/>
				)
			}))
		})
	},[])

	// handleFilter function will receive an event (onChange) from the search input

	const handleFilter = (event) => {

		const searchWord = event.target.value;
		setWordEntered(searchWord)

		fetch('http://localhost:4000/courses/all')
		.then(res=> res.json())
		.then(data => {
				// condition for filtering all the courses name that matches on the searchWord
				const newFilter = data.filter((value)=>{
					return value.name.toLowerCase().includes(searchWord.toLowerCase())
				})
				setCourses(newFilter.map(course => {
						return (

						<CourseCard key = {course.id} courseProp = {course}/>
						)
					
				}))


			
			
		})


	}

return(
	<Fragment>	
	<h1>Courses</h1>

	<div className="search">
      <div className="searchInputs">
        <input
           type="text"
           placeholder="Search course here"
           value={wordEntered}
           onChange={handleFilter}
        />
        <div className="searchIcon">
            <SearchIcon />
        </div>
      </div>
      </div>
	{courses}
	</Fragment>	
	)
}
