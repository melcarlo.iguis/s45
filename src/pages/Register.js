import {useState, useEffect,useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect, useHistory} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Register(){

	const {user} = useContext(UserContext)
	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')

	// State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	const history = useHistory()
	// console.log('email: ' + email)
	// console.log('password1: ' + password1)
	// console.log('password2: ' + password2)

	function registerUser(e){

		e.preventDefault();

		// fetch to check if email exist
		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email : email,	
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

 			// if data is equal to true , it means the email already exist so we put swal alert here for error
 			if(data === true){
 				Swal.fire({
 					title: 'Duplicate email found',
 					icon: 'error',
 					text: 'Please provide different email'
 				})
 			}else{
 				fetch('http://localhost:4000/users/register',{
 					method: 'POST',
 					headers: {
 						'Content-Type' : 'application/json'
 					},
 					body: JSON.stringify({
 						firstName : firstName,
 						lastName : lastName,
 						email : email,
 						mobileNo : mobileNo,
 						password : password1
 					})
 				})
 				.then(res => res.json())
 				.then(data => {
 					console.log(data)

 					if(data=== true){
 						Swal.fire({
 							title: 'Registration successfull',
 							icon: 'success',
 							text: 'Welcome to Zuitt!'
 						})

 						history.push("/login")
 					}
 				})

 			}
 		})
	}

	useEffect(()=> {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){

			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email,password1,password2])

	return(

		(user.id !== null) ?
		<Redirect to='/courses'/>

		:

		<Form onSubmit={(e)=>registerUser(e)}>
			<Form.Group>
			<h2>Register</h2>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type = 'text'
					placeholder = 'Enter first name'
					value= {firstName}
					onChange= {e => setFirstName(e.target.value)}
					required
				/>
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type = 'text'
					placeholder = 'Enter last name'
					value= {lastName}
					onChange= {e => setLastName(e.target.value)}
					required
				/>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type = 'email'
					placeholder = 'Enter email'
					value= {email}
					onChange= {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				   We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group>	
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type = 'text'
					placeholder = 'Enter mobile number'
					value= {mobileNo}
					onChange= {e => setMobileNo(e.target.value)}
					required
					 minlength="11"
				/>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type= 'password'
					placeholder = 'Please input your password here'
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type= 'password'
					placeholder = 'Please verify your Password'
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
			{	isActive ? 
				<Button variant='primary' type='submit' id='submitBtn'>
							Register
				</Button>

				: 
				<Button variant='danger' type='submit' id='submitBtn' disabled>
							Register
				</Button>

			}
		</Form>

	)
}