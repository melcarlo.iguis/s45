const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis harum alias nam temporibus vitae repudiandae explicabo, non ex doloremque voluptate. Minima ea assumenda blanditiis, dolor alias, debitis nostrum a fugiat.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Phython - Django ",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis harum alias nam temporibus vitae repudiandae explicabo, non ex doloremque voluptate. Minima ea assumenda blanditiis, dolor alias, debitis nostrum a fugiat.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Sptringboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis harum alias nam temporibus vitae repudiandae explicabo, non ex doloremque voluptate. Minima ea assumenda blanditiis, dolor alias, debitis nostrum a fugiat.",
		price: 60000,
		onOffer: true
	}
	
]

export default coursesData;