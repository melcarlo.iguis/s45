import {Fragment, useContext} from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext'


export default function AppNavbar(){

	// State - to store user info stored in the login page
	
	// const [user,setUser] = useState(localStorage.getItem("email"));
	// console.log(user)
	const {user} = useContext(UserContext)



	return(
		<Navbar bg="light" expand="lg">
			<Container fluid>
				<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
				<Navbar.Toggle aria-controls="navbarScroll" />
				<Navbar.Collapse id="navbarScroll">
				<Nav
					className="me-auto my-2 my-lg-0"
					style={{ maxHeight: '100px' }}
					navbarScroll
				>
				<Nav.Link as={Link} to="/" exact>Home</Nav.Link>
				<Nav.Link as={Link} to="/courses" exact>Courses</Nav.Link>

				{(user.id !== null) ? 
					<Nav.Link as={Link} to="/logout" exact>logout</Nav.Link>
					:
					<Fragment>
						<Nav.Link as={Link} to="/register" exact>Register</Nav.Link>
						<Nav.Link as={Link} to="/login" exact>Login</Nav.Link>
					</Fragment>

				}
				
				</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
		


		)
}
