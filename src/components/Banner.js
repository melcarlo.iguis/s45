// import {useState} from 'react';
import {Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner() {


	const route = window.location.href;
	// const [route,setRoute] = useState(window.location.href);
	const route1 = route.slice(21,route.length);
	console.log(route1)

	return(
	<Row>

		{(route1 === '/') ?
		
		<Col className = "p-5">
			<h1>Zuitt Coding Bootcamp</h1>
			<p>Opportunities for everyone, everywhere</p>
			<Button as={Link} to="/courses" variant= "primary">Enroll Now</Button>
		</Col>

		:
		
		<Col className = "p-5">
			<h1>404: Page Not found</h1>
			<p>Go back to the <a href='/'>homepage</a> </p>

			
		</Col>
		}
		
	</Row>

	)
}

