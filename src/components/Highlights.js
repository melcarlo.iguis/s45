import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights() {
	return(
	<Row className="my-3">
		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>
							Learn from home
						</h2>
					</Card.Title>
					<Card.Text>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores suscipit ea consequatur maiores, eius quia quasi repudiandae qui, aut ducimus obcaecati impedit. Aut sequi consequatur repellat recusandae ducimus, ab, dolorum?
						</p>
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>
							Study Now, Pay later
						</h2>
					</Card.Title>
					<Card.Text>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores suscipit ea consequatur maiores, eius quia quasi repudiandae qui, aut ducimus obcaecati impedit. Aut sequi consequatur repellat recusandae ducimus, ab, dolorum?
						</p>
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>
							Be Part of Our Community
						</h2>
					</Card.Title>
					<Card.Text>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores suscipit ea consequatur maiores, eius quia quasi repudiandae qui, aut ducimus obcaecati impedit. Aut sequi consequatur repellat recusandae ducimus, ab, dolorum?
						</p>
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
		
	</Row>

	)
}