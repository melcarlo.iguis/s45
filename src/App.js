import {useState,useEffect} from 'react'
// import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import CourseView from './pages/CourseView'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import ErrorPage from './pages/ErrorPage'
import {UserProvider} from './UserContext'
import './App.css';

function App() {

  // state hook for the user state that's define here for a global scope
  // Initialized as an object with properties from the local storage
  // This will be used to store the information and will be used for validating if a user logged in on the top or not
  const [user,setUser] = useState({
      id: null,
      isAdmin: null
  })  



  // function for clearing localStorage
  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=> {
    console.log(user)
    console.log(localStorage)
  },[user])


  // The userProvider component is what allows other components to consume/use our context. Any components which is not wrapped by UserProvider will not have access to the values provided for our context.

  // You can pass data or info to our context by providing a "value" attribute in our userProvider.

 return (
    <UserProvider value={{user,setUser, unsetUser}} >
    <Router>
      <AppNavbar/>
      <Container>
        <Switch>
         <Route exact path ="/" component={Home}/>
         <Route exact path ="/courses" component={Courses}/>
         <Route exact path ="/courses/:courseId" component={CourseView}/>
         <Route exact path ="/login" component={Login}/>
         <Route exact path ="/register" component={Register}/>
         <Route exact path ="/logout" component={Logout}/>
         <Route component={ErrorPage}/>
        </Switch>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;


